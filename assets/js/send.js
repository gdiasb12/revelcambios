$(function () {
    jQuery.validator.addMethod("testEmail", function (value, element) {
        return this.optional(element) || /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/i.test(value);
    }, "Digite e-mail valido.");

    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    }, spOptions = {
        onKeyPress: function (val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };
    $('.celular-input').mask(SPMaskBehavior, spOptions);
    $('.cep').mask('00000-000');

    // Validação de Formularios
    $('#form').validate({
        rules: {
            email: {
                required: true,
                email: true,
                testEmail: true
            },
            nome: {
                required: true,
                minlength: 2
            }
        },
        messages: {
            email: {
                required: "Digite um email.",
                email: "Digite e-mail valido."
            },
            nome:{
                required: "Digite seu nome.",
                minlength: "Digite seu nome completo."
            }
        },
        submitHandler: function (form) {
            var dados = $(form).serialize();

            var host = "/lib/PHPMailer/envia.php";
            var actBtn = $(form).find('button[type="submit"]');
            var oldText = actBtn.text();

            actBtn.text('Enviando...');
            actBtn.attr('disabled', true);

            $.ajax({
                type: "POST",
                url: host,
                async: true,
                dataType : "json",
                data: dados,

                success: function (result) {
                    
                    if (result.status == '200') {
                        window.location = "sucesso";
                    } else {
                        actBtn.text(oldText);
                        actBtn.attr('disabled', false);
                    }

                },
                error: function (data) {
                    actBtn.text(oldText);
                    actBtn.attr('disabled', false);
                }
            });

            return false;
        }
    });


});

