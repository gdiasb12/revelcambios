<? session_start();
   

if($_SESSION['v'] == 'S'){
?>
<html lang="pt-bt">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../assets/img/pagina/favicon.ico" type="image/x-icon"/>
    <title>Revel | ADMINISTRADOR</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="
      https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<?
error_reporting(E_ERROR);
include("../assets/conexao/conexao.php");
  function verif_id()
  {
     if($_GET['_adm'])
     {
        return $_GET['_adm'];
     }
     else
     {
        return 'home';
     }
  }
  $pagina = verif_id();
  ?>

</head>
<body>
  <section id="page_admin" class="col-md-10 col-md-offset-1">

  <header id="topo" class="admin col-xs-12">
    <img id="logo-topo" alt="Logo Revel" class="image col-xs-10 col-xs-offset-1" src="../assets/img/pagina/logo.png">  
    <p id="endereco-topo">Av: Sapopemba, 6.129 - Vila Guarani/SP - Tel. (11) 2115-8500</p>
  </header>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <nav class="col-md-2 menu_admin">
      <ul class="nav">
        <h2>Menu de Operações</h2>
        <li><a href="slide"><span id="e" class="glyphicon glyphicon-pictureglyphicon glyphicon-picture"></span> Slide</a></li>
        <li><a href="quadros"><span id="e" class="glyphicon glyphicon-picture"></span> Quadros</a></li>
        <hr />
        <li><a href="logoff.php"><span id="e" class="glyphicon glyphicon-off"></span> Sair</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

  <section id="container_admin" class="col-md-10">
      <?php if(!@include('./page/'.$pagina.'.adm.php')) @include('./page/home.adm.php'); ?>
  </section>
  <footer id="sec_foot" class="col-xs-12">
  <div class="adm border-white col-xs-12">
  <div class="col-md-12" id="info-rodape">
    <p class="col-md-8" id="direito-copia">
      &copy COPYRIGHT 2014 - Todos os direitos reservados a Revel.
    </p>
    <p class="col-md-4" id="developed">
      Developed by <a href="http://www.agenciasaga.com.br" target="_blank">Agência Saga</a>
    </p>
  </div>
  </div>
  </footer>
  </section>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>
<?
  }else{
  ?>

  		<script>
          	window.location.href="login";
          </script>
  <?
  }
?>
