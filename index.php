<!DOCTYPE html>
<html lang="pt-bt">
<head>
  <?
  error_reporting( E_ERROR);

  include("assets/conexao/conexao.php");
  function verif_id()
  {
    if($_GET['_page'])
    {
      return $_GET['_page'];
    }
    else
    {
      return 'home';
    }
  }
  $pagina = verif_id();
  ?>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <meta http-equiv="content-type" content="text/html; charset=utf-8"> -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta http-equiv="description" content="Revel C&acirc;mbios - Ret&iacute;fica de c&aacute;mbios autom&aacute;ticos e manuais. ">
  <meta http-equiv="keywords" content="c&acirc;mbio autom&aacute;tico, ret&iacute;fica de c&acirc;mbio, c&acirc;mbio e embreagem, c&acirc;mbios automotivos, pe&ccedil;as para c&acirc;mbios, mecanica em geral">
  <meta name="robots" content="index, follow">
  <meta name="distribution" content="global">
  <meta name="language" content="pt-br">
  <link rel="shortcut icon" href="/assets/img/pagina/favicon.ico" type="image/x-icon"/>
  <title>Revel | Ret&iacute;fica de C&acirc;mbios Autom&aacute;ticos e Manuais.</title>

<!-- Bootstrap -->
<style type="text/css">
  <?php include 'assets/css/bootstrap.min.css'; ?>
  <?php include 'assets/css/style.min.css'; ?>
</style>
<!-- <link type="text/css" href="./assets/css/bootstrap.min.css" rel="stylesheet">   -->
<!-- <link type="text/css" href="./assets/css/style.min.css" rel="stylesheet"> -->

<?php if ($pagina == 'home'): ?>
  <link rel="stylesheet" type="text/css" href="./assets/js/slide/style.min.css" />
<?php endif ?>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="
https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript">var rl_siteid = "50c73332-2b29-4c3f-8ef1-ea3c030c9fb0";</script><script type="text/javascript" src="//cdn.rlets.com/capture_static/mms/mms.js" async="async"></script>

</head>
<body>
  <section id="page" class="col-md-10 col-md-offset-1">

    <header id="topo" class="col-xs-12">
      <div id="rede-social" class="col-md-offset-10">
        <a href="https://www.facebook.com/revelcambios" target="_blank"><img alt="Link para Facebook" class="col-md-4" src="/assets/img/pagina/icon-fb.png"> </a> 
        <a href="https://twitter.com/RevelCambios" target="_blank"><img alt="Link para Twitter" class="col-md-4" src="/assets/img/pagina/icon-tw.png"> </a>
        <a href="https://www.youtube.com/channel/UCSWdW-xsQ5z7Wx-MhkaFi_w" target="_blank"><img alt="Link para Youtube" class="col-md-4" src="/assets/img/pagina/icon-yt.png">   </a>  
      </div>
      <img id="logo-topo" alt="Logo Revel" class="image col-xs-10 col-xs-offset-1" src="/assets/img/pagina/logo.png">  
      <p id="endereco-topo">Av: Sapopemba, 6.129 - Vila Guarani/SP - Tel. (11) 2115-8500</p>
    </header>
    <nav class="navbar navbar-default " role="navigation">

      <div class="container-fluid col-md-12">
        <div class="border-white">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Menu</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="/home">HOME</a></li>
              <li><a href="/empresa">EMPRESA</a></li>
              <li><a href="/servicos">SERVI&Ccedil;OS</a></li>
              <li><a href="/pecas">PE&Ccedil;AS</a></li>
              <li><a href="/estrutura">ESTRUTURA</a></li>
              <li><a href="/contato">CONTATOS</a></li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </div>
    </nav>

    <section id="container" class="col-md-12">
      <?php if(! @include('./page/'.$pagina.'.page.php')) @include('./page/home.page.php'); ?>

    </section>
    <footer id="sec_foot" class="col-xs-12">
      <div class="border-white col-xs-12">
        <div class="caixa_foot col-md-4">
          <h4>PARCELAMOS NOS CART&Otilde;ES</h4>
          <div class="col-md-12 img-rodape">
            <img src="/assets/img/pagina/secao_foot_cards.png" width="84%" alt="Cartoes para pagamento">
          </div>
        </div>

        <div class="caixa_foot col-md-4">
          <h4>VENDA DE PE&Ccedil;AS</h4>
          <div class="col-md-12 img-rodape" data-toggle="modal" data-target="#myModal">
            <img src="/assets/img/pagina/secao_foot_vendas.png" width="84%" alt="Cartoes para pagamento">
          </div>
          <!-- Modal -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                  <h4 class="modal-title" id="myModalLabel" style="color: #999;">Veja s&oacute; o que temos pra voc&ecirc; !</h4>
                </div>
                <div class="modal-body">
                  <a href="http://www.bomnegocio.com/loja/id/26465" target="_blank">Fa&ccedil;a um Bom Neg&oacute;cio</a><br />
                  <a href="http://perfil.mercadolivre.com.br/REVELCMBIOS" target="_blank">Mercado Livre</a><br />
                  <a href="http://cidadesaopaulo.olx.com.br/users/RevelCambios" target="_blank">Olx</a><br />
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="caixa_foot col-md-4" id="orcamento">
          <h4>SOLICITE UM OR&Ccedil;AMENTO</h4>
          <form name="htmlform" id="form" method="POST">

            <input type="hidden" name="tipo" class="tipo" value="form-contato">
            <div id="nome-input">
              <div class="input-group">
                <span class="input-group-addon">Nome</span>
                <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome">
              </div>

              <div class="input-group">
                <span class="input-group-addon">@</span>
                <input type="text" class="form-control" name="email" id="email" placeholder="E-Mail">
              </div>

              <textarea name="msg_body" id="msg" class="form-control" placeholder="Mensagem"></textarea>


              <button type="submit"  class="btn btn-default">Enviar</button>
            </div> 
            <div id="response">
            </div>
          </form>
        </div>
        <div class="col-md-12" id="info-rodape">
          <p class="col-md-8" id="direito-copia">
            &copy COPYRIGHT 2014 - Todos os direitos reservados a Revel.
          </p>
          <p class="col-md-4" id="developed">
          Desenvolvido por <a href="https://pt-br.facebook.com/gdiasb12" target="_blank">Gabriel Dias</a><br />
            Design por <a href="https://pt-br.facebook.com/prdiascomunicacao" target="_blank">PRDias Comunica&ccedil;&atilde;o</a>
         <!--  Desenvolvido por <a href="https://pt-br.facebook.com/gdiasb12" target="_blank">Gabriel Dias</a><br />
            Design por <a href="https://pt-br.facebook.com/prdiascomunicacao" target="_blank">PRDias Comunica&ccedil;&atilde;o</a> -->
          </p>
        </div>
      </div>

    </footer>
  </section>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
  <?php if ($pagina == 'home') { ?>
    <script type="text/javascript" src="/assets/js/slide/jquery.js"></script>
    <script type="text/javascript" src="/assets/js/slide/wowslider.js"></script>
    <script type="text/javascript" src="/assets/js/slide/script.js"></script>
  <?php } else { ?>
  <script src="/assets/js/jquery.min.js"></script>
  <?php } ?>
  
  <script src="/assets/js/bootstrap.min.js"></script>
  <script src="/assets/js/jquery.mask.min.js"></script>
  <script src="/assets/js/jquery.validate.min.js"></script>
  <script src="/assets/js/send.min.js"></script>

</body>
</html>