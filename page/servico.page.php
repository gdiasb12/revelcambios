<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Servi&ccedil;os</title>

  </head>
  <body> 
  <section id="cont_servico" class="col-md-12">
       <h3>SERVIÇOS</h3>
         <div class="col-md-8 col-md-offset-2">
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Retífica de Câmbios Manuais, Automáticos e Automatizados (nacionais e importados)</li>
           <div class="row">
           <div class="col-md-6">
            <img style="margin-bottom:1%;" src="assets/img/pagina/servico/1.png">
           </div>
           <div class="col-md-6">
            <img src="assets/img/pagina/servico/2.png">
           </div>
           </div>
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Substituição do Kit Completo de Embreagem (platô, disco e rolamento)</li>
           <div class="row">
           <div class="col-md-6">
            <img style="margin-bottom:1%;" src="assets/img/pagina/servico/3.png">
           </div>
           <div class="col-md-6">
            <img src="assets/img/pagina/servico/4.png">
           </div>
           </div>
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Venda de Peças e Componentes para Câmbios de Diversas Marcas e Modelos</li>
           <div class="row">
           <div class="col-md-6">
            <img style="margin-bottom:1%;" src="assets/img/pagina/servico/5.png">
           </div>
           <div class="col-md-6">
            <img src="assets/img/pagina/servico/6.png">
           </div>
           </div>
        </div>
  </section>
  </body>
</html>