<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>

  <link rel="stylesheet" type="text/css" href="assets/js/slide/style.min.css" />
  <script type="text/javascript" src="assets/js/slide/jquery.js"></script>
  </head>
   <section id="cont_contato" class="col-md-12">
       <h3>CONTATOS</h3>
       <div class="col-md-12">
       <div class="contato col-md-4">
           <h4><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>TELEFONE</h4>
           <p class="col-md-12">Tel.: (11) 2115-8500<br />
                                Nextel: (11) 7727-8176<br />
                                Id: 30*62172<br />
                                Dúvidas Câmbios Automáticos<br /> 
                                Nextel: (11) 7833-9466<br />
                                Id: 80*66230
           </p>
           <h4><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>EMAILS</h4>
           <p class="col-md-12">revel@revelcambios.com<br /><br />

          <strong>Orçamentos</strong><br />
          thiago@revelcambios.com<br /><br />

          <strong>Dúvidas Técnicas</strong><br />
          rogerio@revelcambios.com<br />
           </p>
       </div>
       <div class="contato-right col-md-8">

           <h4><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>LOCALIZAÇÃO</h4>
           <img src="assets/img/pagina/mapa.jpg" alt="Localização Revel">
           <p class="col-md-12">Av: Sapopemba, 6.129 - Vila Guarani/SP - Cep: 03374-001.</p>
       </div>
       </div>
  </section>

  </body>
</html>