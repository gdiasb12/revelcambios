   <section id="cont_empresa" class="col-md-12">
       <h3>EMPRESA</h3>
       <div class="empresa col-md-8">
           <h4><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>MISSÃO</h4>
           <p class="col-md-12">Com profissionais altamente capacitados e equipamentos de última geração, a missão da Revel - Retífica de Câmbios - é fornecer o melhor serviço de retífica, reparação e troca de peças e produtos do sistema de transmissão dos veículos, com o intuito de fidelizar e oferecer o melhor custo-benefício aos seus clientes.
           </p><br /><br />
           <h4><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>VISÃO</h4>
           <p class="col-md-12">Com a crescente demanda dos veículos com câmbios automáticos e automatizados, nossa equipe de profissionais treinada é certificada para fornecer o melhor reparo em todos os conjuntos de transmissão disponíveis no mercado. Nossa visão é sempre estar especializada com as novas tecnologias do setor automotivo.
           </p><br /><br />
           <h4><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>VALORES</h4>
           <p class="col-md-12">Presente no mercado desde 1974 e com mais de 500 m² em nossa sede própria, o valor de conquistar a confiança em um mercado tão competitivo, nos da credibilidade para garantir o melhor serviço aos nossos clientes.
           </p>
       </div>
       <div class="empresa-right col-md-4">
       	<img class="col-md-12" src="assets/img/pagina/empresa/e1.png" alt="Missão da Empresa" /><br />
       	<img class="col-md-12" src="assets/img/pagina/empresa/e2.png" alt="Visão da Empresa" /><br />
       	<img class="col-md-12" src="assets/img/pagina/empresa/e3.png" alt="Valores da Empresa" /><br />
       </div>
  </section>