<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>

  </head>
  <body> 
  <section id="cont_estrutura" class="col-md-12">
       <h3>ESTRUTURA</h3>
       <div class="estrutura col-md-6">
         <ul>
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>5 Elevadores</li>
           <img src="assets/img/pagina/estrutura/1.png">
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Scanners para Diágnosticos Digitais</li>
           <img src="assets/img/pagina/estrutura/2.png">
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Laboratório de Testes e Análises</li>
           <img src="assets/img/pagina/estrutura/3.png">
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Ferramentas de Última Geração</li>
           <img src="assets/img/pagina/estrutura/4.png">
         </ul>
       </div>
  </section>
  </body>
</html>