<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>

  </head>
  <body>
  <section id="cont_pecas" class="col-md-12">
       <h3>PEÇAS</h3>
       <div class="pecas col-md-6">
         <ul>
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Coroa e Pinhão</li>
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Jogo de Juntas</li>
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Anéis Sincronizados</li>
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Engrenagens</li>
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span> Automáticos e Automatizados<br /><span id="c">(Dualogic - Esitronic - Imotion)</span></li>
         </ul>
       </div>
       <div class="pecas col-md-6">
         <ul>
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Luvas Sincronizadoras</li>
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Garfos</li>
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Caixa de Transferência</li>
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Eixo diferencial</li>
           <li><span class="glyphicon glyphicon-cog" style="margin-right:10px;"></span>Kit Banner</li>
         </ul>
       </div>
  </section>
  </body>
</html>